﻿using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace CarsShow.DB
{
    class CarsShowContext : DbContext
    {
        public CarsShowContext() : base("DefaultConnection")
        {

        }

        public DbSet<Personal> Personals { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<Contract> Contracts { get; set; }
    }
}

﻿using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsShow.DB
{
    class CarsShowInitializer : System.Data.Entity.DropCreateDatabaseAlways<CarsShowContext>
    {
        protected override void Seed(CarsShowContext context)
        {
            var personalList = new List<Personal>()
            {
                new Personal {
                    LastName = "Иванов",
                    FirstName = "Иван",
                    SecondName = "Иванович",
                    DateBirthday = new DateTime(1994, 09, 17),
                    Phone = "55905",
                    Address = "Москва, Красная площадь, Кремль №1"
                },
                new Personal
                {
                    LastName = "Петров",
                    FirstName = "Петр",
                    SecondName = "Петрович",
                    DateBirthday = new DateTime(1993, 09, 17),
                    Experience = 3,
                    Phone = "957865",
                    Address = "Красноярск, Красная площадь, Кремль №1"
                }
            };

            personalList.ForEach(s => context.Personals.Add(s));
            context.SaveChanges();

            var clientList = new List<Client>()
            {
                new Client {
                    LastName = "Иванов",
                    FirstName = "Иван",
                    SecondName = "Иванович",
                    Passport = "0408040506",
                    DateBirthday = new DateTime(1994, 09, 17),
                    Phone = "55905",
                    Address = "Москва, Красная площадь, Кремль №1"
                },
                new Client
                {
                    LastName = "Петров",
                    FirstName = "Петр",
                    SecondName = "Петрович",
                    Passport = "0408010203",
                    DateBirthday = new DateTime(1993, 09, 17),
                    Phone = "957865",
                    Address = "Красноярск, Красная площадь, Кремль №1"
                }
            };

            clientList.ForEach(s => context.Clients.Add(s));
            context.SaveChanges();

            Brand toyota = new Brand
            {
                Name = "Toyota",
                Country = "Япония"
            };
            Brand nissan = new Brand
            {
                Name = "Nissan",
                Country = "Япония"
            };
            Brand ford = new Brand
            {
                Name = "Ford",
                Country = "Америка"
            };
            Brand hyundai = new Brand
            {
                Name = "Hyundai",
                Country = "Корея"
            };
            Brand bmv = new Brand
            {
                Name = "BMW",
                Country = "Германия"
            };

            var brandList = new List<Brand>()
            {
                toyota,
                nissan,
                ford,
                hyundai,
                bmv,
            };

            brandList.ForEach(s => context.Brands.Add(s));
            context.SaveChanges();

            Model camry17 = new Model
            {
                Name = "Camry 2.0",
                ShortName = "XV70",
                Body = "седан",
                Door = 4,
                Year = 2017,
                Volume = "1998 куб. см",
                Power = "150 л.с.",
                Fuel = "бензин АИ-92",
                Box = "автомат",
                Brand = toyota,
            };
            Model camry17_2 = new Model
            {
                Name = "Camry 2.5",
                ShortName = "XV70",
                Body = "седан",
                Door = 4,
                Year = 2017,
                Volume = "2494 куб. см",
                Power = "181 л.с.",
                Fuel = "бензин АИ-92",
                Box = "автомат",
                Brand = toyota,
            };
            Model mustang = new Model
            {
                Name = "Mustang",
                ShortName = "GT 5.0",
                Body = "купе",
                Door = 2,
                Year = 2014,
                Volume = "4951 куб. см",
                Power = "426 л.с.",
                Fuel = "бензин АИ-95",
                Box = "механика",
                Brand = ford,
            };

            var modelList = new List<Model>
            {
                camry17,
                camry17_2,
                mustang
            };

            modelList.ForEach(s => context.Models.Add(s));
            context.SaveChanges();

            Car car1 = new Car
            {
                Color = "белый",
                Distance = "16000 км",
                Status = "новый",
                Vin = "1234567890",
                Pts = 0,
                Model = camry17
            };
            Car car2 = new Car
            {
                Color = "серый",
                Distance = "116000 км",
                Status = "новый",
                Vin = "64829648",
                Pts = 3,
                Model = camry17_2
            };

            var carList = new List<Car>
            {
                car1,
                car2
            };

            carList.ForEach(s => context.Cars.Add(s));
            context.SaveChanges();
            Contract contract = new Contract
            {
                Date = DateTime.Today,
                Car = car1,
                Personal = personalList.First(),
                Client = clientList.First()
            };
            Contract contract1 = new Contract
            {
                Date = DateTime.Today,
                Car = car2,
                Personal = personalList.First(),
                Client = clientList.First()
            };

            var contractList = new List<Contract>
            {
                contract,
                contract1
            };

            contractList.ForEach(s => context.Contracts.Add(s));
            context.SaveChanges();
        }
    }
}

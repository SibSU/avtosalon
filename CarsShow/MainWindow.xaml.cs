﻿using CarsShow.DB;
using CarsShow.Views;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarsShow
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<MenuItem> MenuGroupWindow;

        public MainWindow()
        {
            InitializeComponent();
            InitializeMenuGroupWindow();

            menuPersonal.IsChecked = true;
            ChangeSelectedMenuGroupWindow(menuPersonal);

            menuExit.Click += MenuExit_Click;
        }

        private void MenuDb_Click(object sender, RoutedEventArgs e)
        {
            MenuItem selected = sender as MenuItem;

            switch (selected.Name)
            {
                default:
                    MessageBox.Show("Действие не задано");
                    break;
            }
        }

        private void InitializeMenuGroupWindow()
        {
            MenuGroupWindow = new List<MenuItem>();
            MenuGroupWindow.Add(menuCars);
            MenuGroupWindow.Add(menuModel);
            MenuGroupWindow.Add(menuBrand);
            MenuGroupWindow.Add(menuContract);
            MenuGroupWindow.Add(menuClient);
            MenuGroupWindow.Add(menuPersonal);

            foreach (MenuItem item in MenuGroupWindow)
            {
                item.Click += MenuGroupWindow_Click;
                item.IsCheckable = true;
            }
        }

        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void MenuGroupWindow_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            ChangeSelectedMenuGroupWindow(item);
        }

        private void ChangeSelectedMenuGroupWindow(MenuItem selected)
        {
            if (MenuGroupWindow.Find(x => x == selected).IsChecked)
            {
                Loading(true);

                foreach (MenuItem item in MenuGroupWindow)
                {
                    item.IsChecked = false;
                }

                MenuGroupWindow.Find(x => x == selected).IsChecked = true;
                
                switch (selected.Name)
                {
                    case "menuPersonal":
                        FrameContext.Navigate(new PersonalPage());
                        break;
                    case "menuClient":
                        FrameContext.Navigate(new ClientPage());
                        break;
                    case "menuBrand":
                        FrameContext.Navigate(new BrandPage());
                        break;
                    case "menuModel":
                        FrameContext.Navigate(new ModelPage());
                        break;
                    case "menuCars":
                        FrameContext.Navigate(new CarPage());
                        break;
                    case "menuContract":
                        FrameContext.Navigate(new ContractPage());
                        break;
                    default:
                        MessageBox.Show("Действие не задано");
                        break;
                }
            }
        }



        public void Loading(bool type)
        {
            if (type)
            {
                viewLoading.Visibility = Visibility.Visible;
            }
            else
            {
                viewLoading.Visibility = Visibility.Collapsed;
            }
        }
    }
}

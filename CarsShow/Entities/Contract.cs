﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsShow.Entities
{
    [Table("contracts", Schema = "public")]
    public class Contract
    {
        [Key]
        [ForeignKey("Car")]
        [Column("id_contact")]
        public int Id { get; private set; }

        [Column("date")]
        public DateTime Date { get; set; }

        [ForeignKey("Client")]
        [Column("client_id")]
        public int ClientId { get; set; }

        [Required]
        public virtual Client Client { get; set; }

        [ForeignKey("Personal")]
        [Column("personal_id")]
        public int PersonalId { get; set; }

        [Required]
        public virtual Personal Personal { get; set; }
        
        public virtual Car Car { get; set; }
    }
}

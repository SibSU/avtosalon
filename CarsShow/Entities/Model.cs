﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsShow.Entities
{
    [Table("models", Schema = "public")]
    public class Model
    {
        [Key]
        [Column("id_model")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index(IsUnique = true)]
        public int Id { get; private set; }

        [Column("name", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Name { get; set; }

        [Column("short_name", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string ShortName { get; set; }

        [Column("body", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Body { get; set; }

        [Column("door")]
        [Required]
        public int Door { get; set; }

        [Column("year")]
        [Required]
        public int Year { get; set; }

        [Column("volume", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Volume { get; set; }
        
        [Column("power", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Power { get; set; }

        [Column("fuel", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Fuel { get; set; }

        [Column("box", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Box { get; set; }

        [ForeignKey("Brand")]
        public int BrandId { get; set; }

        [Required]
        public virtual Brand Brand { get; set; }

        public virtual ICollection<Car> Cars { get; set; }

        public string ModelName()
        {
            return Name + " " + ShortName;
        }
    }
}

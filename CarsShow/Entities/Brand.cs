﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsShow.Entities
{
    [Table("brands", Schema = "public")]
    public class Brand
    {
        [Key]
        [Column("id_brand")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index(IsUnique = true)]
        public int Id { get; private set; }

        [Column("name", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Index(IsUnique = true)]
        [Required]
        public string Name { get; set; }

        [Column("country", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Country { get; set; }

        public ICollection<Model> Models { get; set; }
    }
}

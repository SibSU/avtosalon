﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsShow.Entities
{
    [Table("cars", Schema = "public")]
    public class Car
    {
        [Key]
        [Column("id_car")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index(IsUnique = true)]
        public int Id { get; private set; }

        [Column("status", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Status { get; set; }

        [Column("color", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Color { get; set; }

        [Column("distance", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Distance { get; set; }

        [Column("vin", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Vin { get; set; }

        [Column("pts")]
        [Required]
        public int Pts { get; set; }

        [ForeignKey("Model")]
        [Column("model_id")]
        public int ModelId { get; set; }

        [Required]
        public virtual Model Model { get; set; }

        public virtual Contract Contract { get; set; }
    }
}

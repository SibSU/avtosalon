﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsShow.Entities
{
    [Table("clients", Schema = "public")]
    public class Client
    {
        [Key]
        [Column("id_client")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Index(IsUnique = true)]
        public int Id { get; private set; }

        [Column("last_name", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string LastName { get; set; }

        [Column("first_name", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string FirstName { get; set; }

        [Column("second_name", TypeName = "VARCHAR")]
        [StringLength(50)]
        public string SecondName { get; set; }

        [Column("passport", TypeName = "VARCHAR")]
        [Index(IsUnique = true)]
        [StringLength(10)]
        [Required]
        public string Passport { get; set; }

        [Column("date_birthday")]
        [Required]
        public DateTime DateBirthday { get; set; }

        [Column("phone", TypeName = "VARCHAR")]
        [StringLength(10)]
        [Required]
        public string Phone { get; set; }

        [Column("address", TypeName = "VARCHAR")]
        [StringLength(50)]
        [Required]
        public string Address { get; set; }
        
        public virtual ICollection<Contract> Contracts { get; set; }
    }
}

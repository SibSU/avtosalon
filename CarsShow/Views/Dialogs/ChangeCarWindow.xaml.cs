﻿using CarsShow.DB;
using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarsShow.Views.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ChangeCarWindow.xaml
    /// </summary>
    public partial class ChangeCarWindow : Window
    {
        private CarsShowContext context;
        private Car car;

        public ChangeCarWindow(Car car = null)
        {
            InitializeComponent();
            this.car = car;

            context = new CarsShowContext();
            DownloadAsync();

            btnOk.Click += BtnOk_Click;
            btnCancel.Click += BtnCancel_Click;

            if (this.car != null)
            {
                this.Title = "Редактировать";
                textStatus.Text = this.car.Status.Trim();
                textColor.Text = this.car.Color.Trim();
                textDistance.Text = this.car.Distance.Trim();
                textVin.Text = this.car.Vin.Trim();
                textPts.Text = this.car.Pts.ToString();

                comboModel.SelectedValue = this.car.ModelId;
            } else
            {
                car = new Car();
                this.Title = "Добавить";
            }
        }

        private async void DownloadAsync()
        {
            comboModel.ItemsSource = await Task.Run(() => context.Models.ToListAsync());
            comboModel.Items.Refresh();
        }

            private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                car.Status = textStatus.Text.Trim();
                car.Color = textColor.Text.Trim();
                car.Distance = textDistance.Text.Trim();
                car.Vin = textVin.Text.Trim();
                car.Pts = Int32.Parse(textPts.Text);

                car.ModelId = (int)comboModel.SelectedValue;
                car.Model = (Model)comboModel.SelectedItem;

                context.Cars.AddOrUpdate(car);
                context.SaveChanges();

                DialogResult = true;
                this.Close();
            } catch (Exception ex)
            {
                MessageBox.Show("Ошибка " + ex.Source + "\n\n" + ex.Message + "\n\nStackTrace: " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

﻿using CarsShow.DB;
using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarsShow.Views.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ChangeContractWindow.xaml
    /// </summary>
    public partial class ChangeContractWindow : Window
    {
        private CarsShowContext context;
        private Contract contract;

        public ChangeContractWindow(Contract contract = null)
        {
            InitializeComponent();
            this.contract = contract;

            context = new CarsShowContext();
            DownloadAsync();

            btnOk.Click += BtnOk_Click;
            btnCancel.Click += BtnCancel_Click;

            if (this.contract != null)
            {
                this.Title = "Редактировать";
                dateContract.SelectedDate = (DateTime) this.contract.Date;

                comboCar.SelectedValue = this.contract.Car.Id;
                comboPersonal.SelectedValue = this.contract.PersonalId;
                comboClient.SelectedValue = this.contract.ClientId;
            } else
            {
                contract = new Contract();
                this.Title = "Добавить";
            }
        }

        private async void DownloadAsync()
        {
            comboCar.ItemsSource = await Task.Run(() => context.Cars.ToListAsync());
            comboCar.Items.Refresh();
            comboPersonal.ItemsSource = await Task.Run(() => context.Personals.ToListAsync());
            comboPersonal.Items.Refresh();
            comboClient.ItemsSource = await Task.Run(() => context.Clients.ToListAsync());
            comboClient.Items.Refresh();
        }

            private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                contract.Date = (DateTime) dateContract.SelectedDate;

                contract.Car = (Car)comboCar.SelectedItem;
                contract.Personal = (Personal)comboPersonal.SelectedItem;
                contract.Client = (Client)comboClient.SelectedItem;

                context.Contracts.AddOrUpdate(contract);
                context.SaveChanges();

                DialogResult = true;
                this.Close();
            } catch (Exception ex)
            {
                MessageBox.Show("Ошибка " + ex.Source + "\n\n" + ex.Message + "\n\nStackTrace: " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

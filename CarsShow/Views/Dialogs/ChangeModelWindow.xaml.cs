﻿using CarsShow.DB;
using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarsShow.Views.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ChangeModelWindow.xaml
    /// </summary>
    public partial class ChangeModelWindow : Window
    {
        private CarsShowContext context;
        private Model model;

        public ChangeModelWindow(Model model = null)
        {
            InitializeComponent();
            this.model = model;

            context = new CarsShowContext();
            DownloadAsync();

            btnOk.Click += BtnOk_Click;
            btnCancel.Click += BtnCancel_Click;

            if (this.model != null)
            {
                this.Title = "Редактировать";
                textName.Text = this.model.Name.Trim();
                textShortName.Text = this.model.ShortName.Trim();
                textBody.Text = this.model.Body.Trim();
                textDoor.Text = this.model.Door.ToString();
                textYear.Text = this.model.Year.ToString();
                textVolume.Text = this.model.Volume.Trim();
                textPower.Text = this.model.Power.Trim();
                textFuel.Text = this.model.Fuel.Trim();
                textBox.Text = this.model.Box.Trim();

                comboBrand.SelectedValue = this.model.BrandId;
            } else
            {
                model = new Model();
                this.Title = "Добавить";
            }
        }

        private async void DownloadAsync()
        {
            comboBrand.ItemsSource = await Task.Run(() => context.Brands.ToListAsync());
            comboBrand.Items.Refresh();
        }

            private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                model.Name = textName.Text.Trim();
                model.ShortName = textShortName.Text.Trim();
                model.Body = textBody.Text.Trim();
                model.Door = Int32.Parse(textDoor.Text);
                model.Year = Int32.Parse(textYear.Text);
                model.Volume = textVolume.Text.Trim();
                model.Power = textPower.Text.Trim();
                model.Fuel = textFuel.Text.Trim();
                model.Box = textBox.Text.Trim();

                model.BrandId = (int)comboBrand.SelectedValue;
                model.Brand = (Brand)comboBrand.SelectedItem;

                context.Models.AddOrUpdate(model);
                context.SaveChanges();

                DialogResult = true;
                this.Close();
            } catch (Exception ex)
            {
                MessageBox.Show("Ошибка " + ex.Source + "\n\n" + ex.Message + "\n\nStackTrace: " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

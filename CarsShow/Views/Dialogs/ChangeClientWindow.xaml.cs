﻿using CarsShow.DB;
using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarsShow.Views.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ChangeclientWindow.xaml
    /// </summary>
    public partial class ChangeClientWindow : Window
    {
        private CarsShowContext context;
        private Client client;

        public ChangeClientWindow(Client client = null)
        {
            InitializeComponent();
            this.client = client;

            context = new CarsShowContext();

            btnOk.Click += BtnOk_Click;
            btnCancel.Click += BtnCancel_Click;

            if (this.client != null)
            {
                this.Title = "Редактировать";
                textLastName.Text = this.client.LastName.Trim();
                textFirstName.Text = this.client.FirstName.Trim();
                textSecondName.Text = this.client.SecondName.Trim();
                textPassport.Text = this.client.Passport.Trim();
                dateBirthday.SelectedDate = this.client.DateBirthday;
                textPhone.Text = this.client.Phone.Trim();
                textAddress.Text = this.client.Address.Trim();
            } else
            {
                this.Title = "Добавить";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (client == null)
                {
                    client = new Client();
                }

                client.LastName = textLastName.Text.Trim();
                client.FirstName= textFirstName.Text.Trim();
                client.SecondName = textSecondName.Text.Trim();
                client.Passport = textPassport.Text.Trim();
                client.DateBirthday = (DateTime) dateBirthday.SelectedDate;
                client.Phone = textPhone.Text;
                client.Address = textAddress.Text;

                context.Clients.AddOrUpdate(client);
                context.SaveChanges();

                DialogResult = true;
                this.Close();
            } catch (Exception ex)
            {
                MessageBox.Show("Ошибка " + ex.Source + "\n\n" + ex.Message + "\n\nStackTrace: " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

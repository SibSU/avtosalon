﻿using CarsShow.DB;
using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarsShow.Views.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ChangebrandWindow.xaml
    /// </summary>
    public partial class ChangeBrandWindow : Window
    {
        private CarsShowContext context;
        private Brand brand;

        public ChangeBrandWindow(Brand brand = null)
        {
            InitializeComponent();
            this.brand = brand;

            context = new CarsShowContext();

            btnOk.Click += BtnOk_Click;
            btnCancel.Click += BtnCancel_Click;

            if (this.brand != null)
            {
                this.Title = "Редактировать";
                textName.Text = this.brand.Name.Trim();
                textCountry.Text = this.brand.Country.Trim();
            } else
            {
                this.Title = "Добавить";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (brand == null)
                {
                    brand = new Brand();
                }

                brand.Name = textName.Text.Trim();
                brand.Country = textCountry.Text.Trim();
                
                context.Brands.AddOrUpdate(brand);
                context.SaveChanges();

                DialogResult = true;
                this.Close();
            } catch (Exception ex)
            {
                MessageBox.Show("Ошибка " + ex.Source + "\n\n" + ex.Message + "\n\nStackTrace: " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

﻿using CarsShow.DB;
using CarsShow.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarsShow.Views.Dialogs
{
    /// <summary>
    /// Логика взаимодействия для ChangePersonalWindow.xaml
    /// </summary>
    public partial class ChangePersonalWindow : Window
    {
        private CarsShowContext context;
        private Personal personal;

        public ChangePersonalWindow(Personal personal = null)
        {
            InitializeComponent();
            this.personal = personal;

            context = new CarsShowContext();

            btnOk.Click += BtnOk_Click;
            btnCancel.Click += BtnCancel_Click;

            if (this.personal != null)
            {
                this.Title = "Редактировать";
                textLastName.Text = this.personal.LastName.Trim();
                textFirstName.Text = this.personal.FirstName.Trim();
                textSecondName.Text = this.personal.SecondName.Trim();
                dateBirthday.SelectedDate = this.personal.DateBirthday;
                textExperience.Text = this.personal.Experience.ToString();
                textPhone.Text = this.personal.Phone.Trim();
                textAddress.Text = this.personal.Address.Trim();
            } else
            {
                this.Title = "Добавить";
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (personal == null)
                {
                    personal = new Personal();
                }

                personal.LastName = textLastName.Text.Trim();
                personal.FirstName= textFirstName.Text.Trim();
                personal.SecondName = textSecondName.Text.Trim();
                personal.DateBirthday = (DateTime) dateBirthday.SelectedDate;
                personal.Experience = Int32.Parse(textExperience.Text);
                personal.Phone = textPhone.Text;
                personal.Address = textAddress.Text;

                context.Personals.AddOrUpdate(personal);
                context.SaveChanges();

                DialogResult = true;
                this.Close();
            } catch (Exception ex)
            {
                MessageBox.Show("Ошибка " + ex.Source + "\n\n" + ex.Message + "\n\nStackTrace: " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}

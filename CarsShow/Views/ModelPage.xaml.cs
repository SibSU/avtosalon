﻿using CarsShow.DB;
using CarsShow.Entities;
using CarsShow.Views.Dialogs;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarsShow.Views
{
    /// <summary>
    /// Логика взаимодействия для PersonalPage.xaml
    /// </summary>
    public partial class ModelPage : Page
    {
        private MainWindow _mainWindow;
        private CarsShowContext context;

        private List<Model> list;

        public ModelPage()
        {
            InitializeComponent();
            InitializeContext();

            UpdateAsync();

            Loaded += Page_Loaded;
            btnUpdate.Click += BtnUpdate_Click;

            btnAdd.Click += BtnAdd_Click;
            btnEdit.Click += BtnEdit_Click;
            btnDelete.Click += BtnDelete_Click;
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (dataModel.SelectedIndex != -1)
            {
                if (MessageBox.Show(
                    "Удалить запись о модели \n" + ((Model)dataModel.SelectedItem).Name.Trim(),
                    "Удаление",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning) == MessageBoxResult.Yes)
                {
                    try
                    {
                        context.Models.Remove((Model)dataModel.SelectedItem);
                        context.SaveChanges();

                        UpdateAsync();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Ошибка " + ex.InnerException.Source + "\n\n" + ex.InnerException.InnerException.Message + "\n\nStackTrace: " + ex.StackTrace, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void BtnEdit_Click(object sender, RoutedEventArgs e)
        {
            if (dataModel.SelectedIndex != -1)
            {
                ChangeModelWindow dialog = new ChangeModelWindow((Model)dataModel.SelectedItem);
                if (dialog.ShowDialog() == true)
                {
                    UpdateAsync();
                }
            }
        }

        private void BtnAdd_Click(object sender, RoutedEventArgs e)
        {
            ChangeModelWindow dialog = new ChangeModelWindow();
            if (dialog.ShowDialog() == true)
            {
                UpdateAsync();
            }
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            this._mainWindow.Loading(true);
            UpdateAsync();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            this._mainWindow = (MainWindow)Window.GetWindow(this);
        }

        /// <summary>
        /// Инициализация контекста
        /// </summary>
        private void InitializeContext()
        {
            context = new CarsShowContext();
        }

        /// <summary>
        /// Обновление таблицы
        /// </summary>
        public async void UpdateAsync()
        {
            list = await Task.Run(() => context.Models.Include(x => x.Brand).ToListAsync());

            dataModel.ItemsSource = list;
            dataModel.Items.Refresh();

            this._mainWindow.Loading(false);
        }
    }
}
